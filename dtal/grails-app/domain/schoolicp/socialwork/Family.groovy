package schoolicp.socialwork


import grails.rest.*

@Resource(readOnly = false, formats = ['json', 'xml'])
class Family {

    String residentialAddress
    String postalCode
    String housingType
    String ownership
    String familyStructure
    String familyNotes

    static hasMany = [ familyMembers: FamilyMember, students: Student ]

    static constraints = {
    }

}