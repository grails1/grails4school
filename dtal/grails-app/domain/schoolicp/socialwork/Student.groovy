package schoolicp.socialwork


import grails.rest.*

@Resource(readOnly = false, formats = ['json', 'xml'])
class Student {
    String name
    Date dateOfBirth

    Integer officerInCharge
    Integer coveringOfficer

    String otherRemarks
    Boolean consent

    String gender
    String race

    static belongsTo = [family: Family]

    static hasOne = [school: School]

    static constraints = {
        family nullable: true
        school unique: true
    }

    static mapping = {
        family lazy: false
    }
}