package schoolicp.socialwork


import grails.rest.*

@Resource(readOnly = false, formats = ['json', 'xml'])
class FamilyMember {
    String name
    String relation
    Boolean mainCareGiver
    String contact
    Boolean sameAddress
    Boolean employed

    static belongsTo = [family: Family]

    static mapping = {
        family lazy: false
    }
}