package schoolicp.socialwork


import grails.rest.*

@Resource(readOnly = false, formats = ['json', 'xml'])
class School {
// Student's school related information
    String name
    String dateOfRequest
    String requestersName
    String requestersEmail
    String requestersContact
    String schoolClass
    String formTeacher
    String currentSupportFromSchool
    String additionalNotes
}